import pandas
import plotly.express


file = r"C:\Users\Anast\Desktop\output_old_1U.txt"

df = pandas.read_csv(file)
print(df)

df[' Initial Altitude (km)'] = df.index + 105.49
print(df)

fig = plotly.express.line(df, x = ' Initial Altitude (km)', y = ' Orbital lifetime (Days)', title='Orbital lifetime of 1U CubeSat vs initial altitude (km)')
fig.show()

